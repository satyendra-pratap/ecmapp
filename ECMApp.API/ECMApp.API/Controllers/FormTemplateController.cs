﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Data.SqlClient;
using System.Linq;
using System.Web; 
using System.Web.Http;
using ECMApp.API.Dto;
using ECMApp.API.Models; 

namespace ECMApp.API.Controllers
{
    [RoutePrefix("api/FormTemplate")]
    public class FormTemplateController : ApiController
    {

        private DBModel db = new DBModel();

        [ActionName("FormControlList")]
        [HttpGet]
        public IEnumerable<procFormControlList_G_Result> GetFormControlList()
        {
            // var data1=  db.Database.SqlQuery<procFormControlList_G_Result>("exec [Form].[procFormControlList_G]").ToList();
            var data = db.procFormControlList_G().ToList();
            return data;
        }

        [ActionName("DataClassList")]
        [HttpGet]
        public IEnumerable<procFormDataClassList_G_Result> GetDataClassList()
        {
            var data = db.procFormDataClassList_G().ToList();
            return data;
        }

        [ActionName("TemplateList")]
        [HttpGet]
        public IEnumerable<procTemplateMaster_G_Result> GetTemplateMasterList(int? templateId)
        {
            var data = db.procTemplateMaster_G(templateId).ToList();
            return data;
        }


        [ActionName("FormTemplateData")]
        [HttpGet]
        public IEnumerable<procFormTemplate_G_Result> GetFormTemplate(int? dataClassId, int? formTemp_Id)
        {
            var data = db.procFormTemplate_G(dataClassId, formTemp_Id).ToList();
            return data;
        }

        [ActionName("FormTemplateOpration")]
        [HttpPost]
        public IHttpActionResult FormTemplate_IUD(FormTemplateDto obj)
        {
            try
            {
                ObjectParameter flag = new ObjectParameter("flag", typeof(int));
                ObjectParameter errorMsg = new ObjectParameter("ErrorMsg", typeof(string));

                var createFormTemplate = db.procFormTemplate_IUD(
                   obj.FormTempId, obj.FormTemplateName, obj.FormDataclassId, obj.TemplateId,
                   obj.Order, obj.TBLClassName, obj.TBLClassCSS, obj.TableName, obj.IsDelete, flag, errorMsg);

                var flagValue = Convert.ToInt32(flag.Value);
                var errorMsgValue = Convert.ToString(errorMsg.Value);
                var returnObject = new { flag = flagValue, errorMsg = errorMsgValue };
                return Ok(returnObject);

            } catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }

        [ActionName("FormFieldMaster")]
        [HttpGet]
        public IEnumerable<procFormFieldMaster_G_Result> GetFormFieldMaster(int? descType, int? formField_Id)
        {
            var data = db.procFormFieldMaster_G(descType, formField_Id).ToList();
            return data;
        }

        [ActionName("FormFieldMasterOpration")]
        [HttpPost]
        public IHttpActionResult FormFieldMaster_IUD(FormFieldMasterDto obj)
        {
            try
            {
                ObjectParameter flag = new ObjectParameter("flag", typeof(int));
                ObjectParameter errorMsg = new ObjectParameter("ErrorMsg", typeof(string));

                var data = db.procFormFieldMaster_IUD(obj.DescType, obj.DataClassId, obj.FormField_Id, obj.Field_Name,obj.FormTemp_Id,
                    obj.Control_Id, obj.OrderNo, obj.AutoFillTable, obj.Field_Param, obj.Column_Name, obj.IsReadOnly, obj.DataType_Id,
                    obj.ValueType, obj.Min_Length, obj.Max_Length, obj.Formula, obj.Handler, obj.Dependent_DescriptorId, obj.IsMandatory,
                    obj.Descriptor_Id, obj.AutoFillScript, obj.IsTableColumn, obj.HTMLText, obj.DefaultValue, obj.DataScript, obj.StaticControl_Name, 
                    obj.DataLoadScript, obj.DataFormat, obj.TDClassName, obj.TDClassCSS, obj.ControlClassName, obj.ControlClassCSS, obj.ValidationRule,
                    obj.DuplicateHandler, obj.FixedColumnTable, obj.Field_ColSpan, obj.Min_Value, obj.Max_Value, obj.SLNO, obj.DuplicateData, obj.Visible,
                    obj.RefDescriptor, obj.RefHandler, obj.DecimalDataTypeBig, obj.RangeValidation, obj.ParentDescriptor_Id, obj.ParentDescAutoFill_Id,
                    obj.Prefix, obj.Suffix, obj.IsDelete, flag, errorMsg
                    );
                var flagValue = Convert.ToInt32(flag.Value);
                var errorMsgValue = Convert.ToString(errorMsg.Value);
                var returnObject = new { flag = flagValue, errorMsg = errorMsgValue };
                return Ok(returnObject);
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }
    }
}
