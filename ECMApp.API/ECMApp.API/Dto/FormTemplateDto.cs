﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ECMApp.API.Dto
{
    public class FormTemplateDto
    {
        public int? FormTempId { get; set; }
        public string FormTemplateName { get; set; }
        public int? FormDataclassId { get; set; }
        public int? TemplateId { get; set; }
        public int? Order { get; set; }
        public string TBLClassName { get; set; }
        public string TBLClassCSS { get; set; }
        public string TableName { get; set; }
        public bool IsDelete { get; set; }
    }
}