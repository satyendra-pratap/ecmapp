﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ECMApp.API.Dto
{
    public class FormFieldMasterDto
    {
        public int? DescType { get; set; }
        public int? DataClassId { get; set; }
        public int FormField_Id { get; set; }
        public string Field_Name { get; set; }
        public int? FormTemp_Id { get; set; }
        public int? Control_Id { get; set; }
        public int? OrderNo { get; set; }
        public string AutoFillTable { get; set; }
        public string Field_Param { get; set; }
        public string Column_Name { get; set; }
        public bool? IsReadOnly { get; set; }
        public int? DataType_Id { get; set; }
        public int? ValueType { get; set; }
        public int? Min_Length { get; set; }
        public int? Max_Length { get; set; } 
        public string Formula { get; set; }
        public bool? Handler { get; set; }
        public int? Dependent_DescriptorId { get; set; }
        public bool? IsMandatory { get; set; }
        public int? Descriptor_Id { get; set; }
        public string AutoFillScript { get; set; }
        public bool? IsTableColumn { get; set; }
        public string HTMLText { get; set; }
        public string DefaultValue { get; set; }
        public string DataScript { get; set; }
        public string StaticControl_Name { get; set; }
        public string DataLoadScript { get; set; }
        public string DataFormat { get; set; }
        public string TDClassName { get; set; }

        public string TDClassCSS { get; set; }
        public string ControlClassName { get; set; }
        public string ControlClassCSS { get; set; }
        public string ValidationRule { get; set; }
        public int? DuplicateHandler { get; set; }
        public bool? FixedColumnTable { get; set; }
        public string Field_ColSpan { get; set; }
        public long? Min_Value { get; set; }
        public long? Max_Value { get; set; }
        public bool? SLNO { get; set; }
        public byte? DuplicateData { get; set; }
        public byte? Visible { get; set; }
        public long? RefDescriptor { get; set; }
        public byte? RefHandler { get; set; }

        public byte? DecimalDataTypeBig { get; set; }

        public string RangeValidation { get; set; }
        public int ParentDescriptor_Id { get; set; }
        public long ParentDescAutoFill_Id { get; set; }
        public string Prefix { get; set; }
        public string Suffix { get; set; }
        public bool IsDelete { get; set; } 
    }
}