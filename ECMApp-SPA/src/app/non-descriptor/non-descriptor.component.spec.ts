import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NonDescriptorComponent } from './non-descriptor.component';

describe('NonDescriptorComponent', () => {
  let component: NonDescriptorComponent;
  let fixture: ComponentFixture<NonDescriptorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NonDescriptorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NonDescriptorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
