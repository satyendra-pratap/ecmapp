export class FormTemplate {
    FormTempId: number;
    FormTemplateName: string;
    FormDataclassId: number;
    TemplateId: number;
    Order: number;
    TBLClassName: string;
    TBLClassCSS: string;
    TableName: string;
    IsDelete: boolean;
}
