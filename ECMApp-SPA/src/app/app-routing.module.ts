import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EmployeesComponent } from './employees/employees.component';
import { FormTemplateComponent } from './form-template/form-template.component';
import { NonDescriptorComponent } from './non-descriptor/non-descriptor.component';
import { DescriptorComponent } from './descriptor/descriptor.component';

const routes: Routes = [
  { path: '', component: FormTemplateComponent},
  { path: 'form-template', component: FormTemplateComponent},
  { path: 'non-descriptor', component: NonDescriptorComponent},
  { path: 'descriptor', component: DescriptorComponent},
  { path: 'employees', component: EmployeesComponent },
  { path: '**', redirectTo: '', pathMatch: 'full' }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
