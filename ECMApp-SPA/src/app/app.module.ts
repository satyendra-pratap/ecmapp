import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { EmployeesComponent } from './employees/employees.component';
import { EmployeeComponent } from './employees/employee/employee.component';
import { EmployeeListComponent } from './employees/employee-list/employee-list.component';
import { EmployeeService } from './_service/employee.service';
import { AlertifyService } from './_service/alertify.service';
import { NavComponent } from './nav/nav.component';
import { FormTemplateComponent } from './form-template/form-template.component';
import { NonDescriptorComponent } from './non-descriptor/non-descriptor.component';
import { DescriptorComponent } from './descriptor/descriptor.component';
import { ErrorInterceptorProvider } from './_service/error.interceptor';
import { FormTemplateService } from './_service/form-template.service';

@NgModule({
  declarations: [
    AppComponent,
    EmployeesComponent,
    EmployeeComponent,
    EmployeeListComponent,
    NavComponent,
    FormTemplateComponent,
    NonDescriptorComponent,
    DescriptorComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    BsDropdownModule.forRoot()
  ],
  providers: [
              EmployeeService,
              AlertifyService,
              ErrorInterceptorProvider,
              FormTemplateService
             ],
  bootstrap: [AppComponent]
})
export class AppModule { }
