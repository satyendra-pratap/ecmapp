import { Component, OnInit } from '@angular/core';
import { EmployeeService } from 'src/app/_service/employee.service';
import { NgForm } from '@angular/forms';
import { AlertifyService } from 'src/app/_service/alertify.service';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent implements OnInit {
  model: any = {};
  constructor(public employeeService: EmployeeService, private alertify: AlertifyService, private toastr: ToastrService) { }

  ngOnInit() {
    this.resetForm();
  }

  onSubmit(employeeForm: NgForm) {
    if (employeeForm.value.EmployeeId == null) {
      this.insertRecord(employeeForm);
    } else {
      this.updateRecord(employeeForm);
    }
  }

  insertRecord(employeeForm: NgForm) {
    this.employeeService.postEmployee(employeeForm.value)
    .subscribe(response => {
      this.toastr.success('Registerd Record sucessfully');
      this.resetForm(employeeForm);
      this.employeeService.refreshList();
    }, error => {
      this.toastr.error(error);
    });
    // console.log(employeeForm);
  }

  updateRecord(employeeForm: NgForm) {
    this.employeeService.putEmployee(employeeForm.value).subscribe( UpdateRes => {
      this.toastr.success('Update Record Sucessfully');
      this.resetForm(employeeForm);
      this.employeeService.refreshList();
    }, error => {
      this.toastr.error(error);
    });
  }

  deleteRecord(employeeForm: NgForm) {
    this.employeeService.deleteEmployee(employeeForm.value).subscribe( deleteRes => {
      this.toastr.success('Delete Record Successfully');
      this.employeeService.refreshList();
    }, error => {
      this.toastr.error(error);
    });
  }

  resetForm(employeeForm?: NgForm) {
    if (employeeForm != null) {
      employeeForm.resetForm();
    }

    this.employeeService.formData = {
      EmployeeId: null,
      FullName: '',
      Position: '',
      EMPCode: '',
      Mobile: ''
    };
  }
}
