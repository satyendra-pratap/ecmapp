import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { EmployeeService } from 'src/app/_service/employee.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.css']
})
export class EmployeeListComponent implements OnInit {

  values: any;
  constructor(private http: HttpClient, private employeeService: EmployeeService, private toastr: ToastrService) { }

  ngOnInit() {
    this.employeeService.refreshList();
  }

  // getValues() {
  //   this.http.get('http://localhost:53746/api/Employees').subscribe( response => {
  //     this.values = response;
  //   }, error => {
  //     console.log(error);
  //   }
  //   );
  // }

  populateForm(value: any) {
      this.employeeService.formData = Object.assign({}, value);
  }

  deleteRecord(id: number) {
    if (confirm('Are you sure to delete this?')) {
      this.employeeService.deleteEmployee(id).subscribe( deleteRes => {
        this.toastr.info('Delete Record Successfully');
        this.employeeService.refreshList();
      }, error => {
        this.toastr.error(error);
      });
    }
  }
}
