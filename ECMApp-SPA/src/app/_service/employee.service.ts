import { Injectable } from '@angular/core';
import { Employee } from '../_model/employee.model';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  formData: Employee;
  list: any;
  readonly baseUrl = 'http://localhost:53746/api/Employees/';
  constructor(private http: HttpClient) { }

  refreshList() {
    this.http.get(this.baseUrl + 'GetEmployees').subscribe( response => {
      this.list = response as Employee ;
    }, error => {
      console.log(error);
    });
  }

  postEmployee(formData: Employee) {
    return this.http.post(this.baseUrl + 'PostEmployee/', formData);
  }

  putEmployee(formData: Employee) {
    return this.http.put(this.baseUrl + 'PutEmployee/' + formData.EmployeeId, formData);
  }

  deleteEmployee(id: number) {
    return this.http.delete(this.baseUrl + 'DeleteEmployee/' + id, );
  }

}
