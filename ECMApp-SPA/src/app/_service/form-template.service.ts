import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormTemplate } from '../_model/form-template.model';
import { map } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';


@Injectable({
  providedIn: 'root'
})
export class FormTemplateService {
   formTemplateData: FormTemplate;
   DataClassList: any;
   templateList: any;
   dataResponse: any;
   formTemplateDataRes: any;
   readonly baseUrl = 'http://localhost:53746/api/FormTemplate/';
   constructor(private http: HttpClient, private toastrService: ToastrService) { }

    dataList() {
      this.http.get(this.baseUrl + 'DataClassList').subscribe( response => {
        this.DataClassList = response;
      }, error => {
        console.log(error);
      });
    }

    TemplateList(templateId: number) {
      this.http.get(this.baseUrl + 'TemplateList?templateId=' + templateId ).subscribe( response => {
        this.templateList = response;
      }, error => {
        console.log(error);
      });
    }

    FormTemplateOpration(formTemplateData: FormTemplate) {
      debugger;
      return this.http.post(this.baseUrl + 'FormTemplateOpration', formTemplateData).pipe(
        map((response: any) => {
          this.dataResponse = response;
        })
      );
     }

     FormTemplateData() {
      this.http.get(this.baseUrl + 'FormTemplateData?dataClassId=null&formTemp_Id=null').subscribe( response => {
        this.formTemplateDataRes = response;
      }, error => {
        console.log(error);
      });
     }
}
