import { Component, OnInit } from '@angular/core';
import { FormTemplateService } from '../_service/form-template.service';
import { NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-form-template',
  templateUrl: './form-template.component.html',
  styleUrls: ['./form-template.component.css']
})
export class FormTemplateComponent implements OnInit {

  constructor(public formTempleateService: FormTemplateService, private toastr: ToastrService, private router: Router) { }
//   model: any = {
//     DataClass_ID : null,
//     DataClass_Name: '',
//  }
  model: any = {};
  dataClassId: any = null;
  dataClassName: any = '';
  showDataClass: any = true;
  showFormTemplate: any = false;
  showFormTemplateList: any = true;
  title = 'Form Template';

  formValue: any = {
    FormDataclassId: 30,
    FormTempId: 85,
    FormTemplateName: 'gg',
    Order: 5,
    TBLClassCSS: 'gg',
    TBLClassName: 'gg',
    TemplateId: 1
};
  ngOnInit() {
    this.resetForm();
    this.formTempleateService.dataList();
    this.formTempleateService.TemplateList(null);
  }

  selectDataClass(data?: NgForm) {
    debugger;
    console.log(this.model);
    this.formTempleateService.formTemplateData.FormDataclassId = this.model.dataClass.DataClass_ID;
    if (this.model) {
      this.showDataClass = false;
      this.showFormTemplate = true;
      this.showFormTemplateList = false;
      // this.dataClassId = this.model.dataClass.DataClass_ID;
      // this.dataClassName = this.model.dataClass.DataClass_Name;

      // this.formTempleateService.TemplateList(null);
    }
  }

  resetForm(formTemplate?: NgForm) {
    debugger;
    if (formTemplate != null) {
      formTemplate.resetForm();
    }
    this.showDataClass = true;
    this.showFormTemplate = false;
    this.showFormTemplateList = true;
    this.model.dataClass = null;

    this.formTempleateService.formTemplateData = {
        FormTempId: null,
        FormTemplateName: '',
        FormDataclassId: null,
        TemplateId: null,
        Order: null,
        TBLClassName: '',
        TBLClassCSS: '',
        TableName: '',
        IsDelete: false,
    };
  }

  onSubmit(formTemplate: NgForm) {
    if (formTemplate.value.FormTempId == null) {
      this.insertFormTemp(formTemplate);
    } else {
      this.updateFormTemp(formTemplate);
    }
  }

  insertFormTemp(formTemplate: NgForm) {
    this.formTempleateService.FormTemplateOpration(formTemplate.value).subscribe(response => {
      if (this.formTempleateService.dataResponse.errorMsg) {
        this.toastr.warning(this.formTempleateService.dataResponse.errorMsg);
      } else {
        this.toastr.success('Insert Record sucessfully');
        this.resetForm(formTemplate);
      }
    }, error => {
      this.toastr.error(error);
    }, () => {
      this.router.navigate(['/form-template']);
    });
  }

  populateFormTemp() {
    this.showDataClass = false;
    this.showFormTemplate = true;
    this.showFormTemplateList = false;
    this.formTempleateService.formTemplateData = Object.assign({}, this.formValue);
  }
  updateFormTemp(formTemplate: NgForm) {
    this.formTempleateService.FormTemplateOpration(formTemplate.value).subscribe(response => {
      if (this.formTempleateService.dataResponse.errorMsg) {
        this.toastr.warning(this.formTempleateService.dataResponse.errorMsg);
      } else {
        this.toastr.success('Update Record Sucessfully');
        this.resetForm(formTemplate);
      }
    }, error => {
      this.toastr.error(error);
    });
  }

  deleteFormTempReco() {
    debugger;
    this.formTempleateService.formTemplateData.FormTempId = 87;
    this.formTempleateService.formTemplateData.IsDelete = true;
    this.formTempleateService.FormTemplateOpration(this.formTempleateService.formTemplateData).subscribe(response => {
      if (this.formTempleateService.dataResponse.errorMsg) {
        this.toastr.warning(this.formTempleateService.dataResponse.errorMsg);
      } else {
        this.toastr.success('Delete Record Sucessfully');
      }
    }, error => {
      this.toastr.error(error);
    });
  }


}
